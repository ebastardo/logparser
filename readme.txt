
Deliverables
------------

1) Java program that can be run from command line

To run this program from the command line you must do the followings:

	1.1 create a mysql conection with the following properties:

	     user: root
	     pass: effuerte05
	     port: 3306

	     for more information check the persistence.xml file which contains the conections settings:
		     <properties>
	            <property name="javax.persistence.jdbc.url" value="jdbc:mysql://localhost:3306/logs?zeroDateTimeBehavior=convertToNull"/>
	            <property name="javax.persistence.jdbc.user" value="root"/>
	            <property name="javax.persistence.jdbc.driver" value="com.mysql.jdbc.Driver"/>
	            <property name="javax.persistence.jdbc.password" value="effuerte05"/>
	            <property name="javax.persistence.schema-generation.database.action" value="create"/>
	         </properties>

    1.2 Run the following script to create the database and tables(sql DBscript is located in the script folder

    ====================================================================================================

	    CREATE DATABASE  IF NOT EXISTS `logs` /*!40100 DEFAULT CHARACTER SET latin1 */;
	    USE `logs`;

		    DROP TABLE IF EXISTS `filter_daily`;
		/*!40101 SET @saved_cs_client     = @@character_set_client */;
		/*!40101 SET character_set_client = utf8 */;
		CREATE TABLE `filter_daily` (
		  `filterdailyId` int(11) NOT NULL AUTO_INCREMENT,
		  `ipAddress` varchar(45) DEFAULT NULL,
		  `date` datetime DEFAULT NULL,
		  `status` varchar(45) DEFAULT NULL,
		  `comment` varchar(45) DEFAULT NULL,
		  `request` varchar(45) DEFAULT NULL,
		  PRIMARY KEY (`filterdailyId`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;

		LOCK TABLES `filter_daily` WRITE;
	/*!40000 ALTER TABLE `filter_daily` DISABLE KEYS */;
	/*!40000 ALTER TABLE `filter_daily` ENABLE KEYS */;
	UNLOCK TABLES;

	DROP TABLE IF EXISTS `filter_hourly`;
	/*!40101 SET @saved_cs_client     = @@character_set_client */;
	/*!40101 SET character_set_client = utf8 */;
	CREATE TABLE `filter_hourly` (
	  `filterhourId` int(11) NOT NULL AUTO_INCREMENT,
	  `ipAddress` varchar(45) DEFAULT NULL,
	  `date` datetime DEFAULT NULL,
	  `status` varchar(45) DEFAULT NULL,
	  `comment` varchar(45) DEFAULT NULL,
	  `request` varchar(45) DEFAULT NULL,
	  PRIMARY KEY (`filterhourId`)
	) ENGINE=InnoDB DEFAULT CHARSET=latin1;

	LOCK TABLES `filter_hourly` WRITE;
	/*!40000 ALTER TABLE `filter_hourly` DISABLE KEYS */;
	/*!40000 ALTER TABLE `filter_hourly` ENABLE KEYS */;
	UNLOCK TABLES;
==============================================================================================

	1.3 Go to the parse folder in the project directory,there you will find the "parser.jar"

	1.4 Run it with the command the following command :

	    java -cp "parser.jar" com.ef.Parser --accesslog=/path/to/file --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100

2) SQL queries for SQL test:

	2.1)Write MySQL query to find IPs that mode more than a certain number of requests for a given time period.

		SELECT ipAddress FROM  filter_hourly 
		WHERE date BETWEEN '2017-01-01 15:00:00' AND '2017-01-01 15:2:59'+ INTERVAL 1 HOUR
		GROUP BY ipAddress HAVING count(ipAddress) >= 1;

		SELECT ipAddress FROM  filter_daily 
		WHERE date BETWEEN '2017-01-01 15:00:00' AND '2017-01-01 15:2:59'+ INTERVAL 1 HOUR
		GROUP BY ipAddress HAVING count(ipAddress) >= 1;

	2.2) #Write MySQL query to find requests made by a given IP

	
		SELECT f.* from filter_hourly f WHERE f.ipAddress = '192.168.11.231';

	
		SELECT f.* from filter_daily f WHERE f.ipAddress = '192.168.11.231';




