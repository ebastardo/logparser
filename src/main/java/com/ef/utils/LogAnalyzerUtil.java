/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef.utils;

import com.ef.model.InputEntry;
import com.ef.model.LogEntry;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author erwin
 */
public class LogAnalyzerUtil {


    public static List<LogEntry> loadLogs(String fileName) {

        File file = new File(fileName);
        List<LogEntry> entries = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String eachLine;
            while ((eachLine = br.readLine()) != null) {
                String[] values = eachLine.split("\\|");

                LogEntry entry = new LogEntry(formatDate(values[0]), values[1], values[2], values[3], getComment(Integer.parseInt(values[3])));
                entries.add(entry);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(LogAnalyzerUtil.class.getName()).log(Level.SEVERE, null, ex);

        } catch (IOException ex) {
        }
        return entries;
    }

    private static String getComment(int statusCode) {
        String comment;
        switch (statusCode) {
            case 400:
                comment = "400 Bad Request";
                break;
            case 401:
                comment = "401 Unauthorized";
                break;
            case 402:
                comment = "402 Payment Required";
                break;
            case 403:
                comment = "403 Forbidden";
                break;
            case 404:
                comment = "404 Not Found";
                break;
            case 405:
                comment = "405 Method Not Allowed";
                break;
            case 406:
                comment = "406 Not Acceptable";
                break;
            case 407:
                comment = "407 Proxy Authentication Required";
            case 408:
                comment = "408 Request Timeout";
                break;
            case 409:
                comment = "409 Conflict";
                break;
            case 410:
                comment = "410 Gone";
                break;
            case 411:
                comment = "411 Length Required";
                break;
            case 412:
                comment = "412 Precondition Failed";
                break;
            case 413:
                comment = "413 Request Entity Too Large";
                break;
            case 414:
                comment = "414 Request-URI Too Long";
                break;
            case 415:
                comment = "415 Unsupported Media Type";
                break;
            case 416:
                comment = "416 Requested Range Not Satisfiable";
                break;
            case 417:
                comment = "417 Expectation Failed";
                break;
            case 418:
                comment = "418 I'm a teapot (RFC 2324)";
                break;
            case 420:
                comment = "420 Enhance Your Calm (Twitter)";
                break;
            case 422:
                comment = "422 Unprocessable Entity (WebDAV)";
                break;
            case 423:
                comment = "423 Locked (WebDAV)";
                break;
            case 424:
                comment = "424 Failed Dependency (WebDAV)";
                break;
            case 425:
                comment = "425 Reserved for WebDAV";
                break;
            case 426:
                comment = "426 Upgrade Required";
                break;
            case 428:
                comment = "428 Precondition Required";
                break;
            case 429:
                comment = "429 Too Many Requests";
                break;
            case 431:
                comment = "431 Request Header Fields Too Large";
                break;
            case 444:
                comment = "444 No Response (Nginx)";
                break;
            case 449:
                comment = "449 Retry With (Microsoft)";
                break;
            case 450:
                comment = "450 Blocked by Windows Parental Controls (Microsoft)";
                break;
            case 451:
                comment = "451 Unavailable For Legal Reasons";
                break;
            case 499:
                comment = "499 Client Closed Request (Nginx)";
            default:
                comment = "Not Blocked";
        }

        return comment;
    }

    private static Date formatDate(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        try {
            return dateFormat.parse(date);
        } catch (ParseException ex) {
            Logger.getLogger(LogAnalyzerUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static LogEntry filterLogs(List<LogEntry> logs, String stardate, String duration, int threshold) {

        Map<LogEntry, Integer> logEntries = new HashMap<>();
        Date fromDate = formatDate(stardate);
        Date toDate = addHoursToDate(fromDate, duration);
        for (LogEntry log : logs) {
            if (log.getDate().after(fromDate) && log.getDate().before(toDate)) {

                if (logEntries.containsKey(log)) {
                    logEntries.put(log, logEntries.get(log) + 1);
                } else {
                    logEntries.put(log, 1);
                }
            }
        }

        for (Map.Entry<LogEntry, Integer> log : logEntries.entrySet()) {
            if (log.getValue() > threshold) {
                return log.getKey();
            }
        }
        return null;
    }

    public static List<LogEntry> filterLogsByIpAddress(List<LogEntry> logs, String stardate, String duration, String ipAddress) {

        List<LogEntry> entries = new ArrayList<>();
        Date fromDate = formatDate(stardate);
        Date toDate = addHoursToDate(fromDate, duration);
        for (LogEntry log : logs) {
            if (log.getDate().after(fromDate) && log.getDate().before(toDate)) {
                if (log.getIpAddress().trim().equalsIgnoreCase(ipAddress)) {
                    entries.add(log);
                }

            }
        }
        //logs logs to console
        System.out.println("===============================================================================================================================================================");
        System.out.println("===============================================================================================================================================================");
        System.out.println("=================================================================Loging Logs that meets the criteria===========================================================");
        System.out.println("===============================================================================================================================================================");
        System.out.println("===============================================================================================================================================================");
        for (LogEntry entry : entries) {
            System.out.println(entry);
        }
        System.out.println("===============================================================================================================================================================");
        System.out.println("===============================================================================================================================================================");

        return entries;
    }

    private static Date addHoursToDate(Date actualDate, String duration) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(actualDate);
        if (duration.equalsIgnoreCase("hourly")) {
            cal.add(Calendar.HOUR_OF_DAY, 1);
        } else {
            cal.add(Calendar.HOUR, 23);
            cal.add(Calendar.MINUTE, 59);
            cal.add(Calendar.SECOND, 59);
        }

        return cal.getTime();
    }

    public static InputEntry buildInputEntry(String[] args) {
        if (args.length < 4) {
            System.out.println("--------------------------------------");
            System.out.println("--------------------------------------");
            System.out.println("please provide the all parameters:");
            System.out.println("--accesslog=/path/to/file");
            System.out.println("--duration=hourly");
            System.out.println("--threshold=100");
            System.out.println("--startDate=2017-01-01.13:00:00");
            System.out.println("--------------------------------------");
            System.out.println("--------------------------------------");
            System.exit(0);
        }
        InputEntry input = new InputEntry();
        for (String param : args) {
            String[] eachParam = param.split("=");
            switch (eachParam[0]) {
                case "--accesslog":
                    input.setFile(eachParam[1]);
                    break;
                case "--startDate":
                    input.setStarDate(formatInputDate(eachParam[1]));
                    break;
                case "--duration":
                    input.setDuration(eachParam[1]);
                    break;
                case "--threshold":
                    input.setThreshold(Integer.valueOf(eachParam[1]));
                    break;
            }
        }

        return input;
    }

    private static String formatInputDate(String s) {
        String[] values = s.split("\\.");
        return values[0] + " " + values[1];
    }
}
