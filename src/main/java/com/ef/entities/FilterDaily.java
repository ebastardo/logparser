/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author erwin
 */
@Entity
@Table(name = "filter_daily")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FilterDaily.findAll", query = "SELECT f FROM FilterDaily f"),
    @NamedQuery(name = "FilterDaily.findByFilterdailyId", query = "SELECT f FROM FilterDaily f WHERE f.filterdailyId = :filterdailyId"),
    @NamedQuery(name = "FilterDaily.findByIpAddress", query = "SELECT f FROM FilterDaily f WHERE f.ipAddress = :ipAddress"),
    @NamedQuery(name = "FilterDaily.findByDate", query = "SELECT f FROM FilterDaily f WHERE f.date = :date"),
    @NamedQuery(name = "FilterDaily.findByStatus", query = "SELECT f FROM FilterDaily f WHERE f.status = :status"),
    @NamedQuery(name = "FilterDaily.findByComment", query = "SELECT f FROM FilterDaily f WHERE f.comment = :comment"),
    @NamedQuery(name = "FilterDaily.findByRequest", query = "SELECT f FROM FilterDaily f WHERE f.request = :request")})
public class FilterDaily implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "filterdailyId")
    private Integer filterdailyId;
    @Column(name = "ipAddress")
    private String ipAddress;
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Column(name = "status")
    private String status;
    @Column(name = "comment")
    private String comment;
    @Column(name = "request")
    private String request;

    public FilterDaily() {
    }

    public FilterDaily(Integer filterdailyId) {
        this.filterdailyId = filterdailyId;
    }

    public Integer getFilterdailyId() {
        return filterdailyId;
    }

    public void setFilterdailyId(Integer filterdailyId) {
        this.filterdailyId = filterdailyId;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (filterdailyId != null ? filterdailyId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FilterDaily)) {
            return false;
        }
        FilterDaily other = (FilterDaily) object;
        if ((this.filterdailyId == null && other.filterdailyId != null) || (this.filterdailyId != null && !this.filterdailyId.equals(other.filterdailyId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ef.entities.FilterDaily[ filterdailyId=" + filterdailyId + " ]";
    }
    
}
