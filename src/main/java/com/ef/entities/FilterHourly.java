/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author erwin
 */
@Entity
@Table(name = "filter_hourly")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FilterHourly.findAll", query = "SELECT f FROM FilterHourly f"),
    @NamedQuery(name = "FilterHourly.findByFilterhourId", query = "SELECT f FROM FilterHourly f WHERE f.filterhourId = :filterhourId"),
    @NamedQuery(name = "FilterHourly.findByIpAddress", query = "SELECT f FROM FilterHourly f WHERE f.ipAddress = :ipAddress"),
    @NamedQuery(name = "FilterHourly.findByDate", query = "SELECT f FROM FilterHourly f WHERE f.date = :date"),
    @NamedQuery(name = "FilterHourly.findByStatus", query = "SELECT f FROM FilterHourly f WHERE f.status = :status"),
    @NamedQuery(name = "FilterHourly.findByComment", query = "SELECT f FROM FilterHourly f WHERE f.comment = :comment"),
    @NamedQuery(name = "FilterHourly.findByRequest", query = "SELECT f FROM FilterHourly f WHERE f.request = :request")})
public class FilterHourly implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "filterhourId")
    private Integer filterhourId;
    @Column(name = "ipAddress")
    private String ipAddress;
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Column(name = "status")
    private String status;
    @Column(name = "comment")
    private String comment;
    @Column(name = "request")
    private String request;

    public FilterHourly() {
    }

    public FilterHourly(Integer filterhourId) {
        this.filterhourId = filterhourId;
    }

    public Integer getFilterhourId() {
        return filterhourId;
    }

    public void setFilterhourId(Integer filterhourId) {
        this.filterhourId = filterhourId;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (filterhourId != null ? filterhourId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FilterHourly)) {
            return false;
        }
        FilterHourly other = (FilterHourly) object;
        if ((this.filterhourId == null && other.filterhourId != null) || (this.filterhourId != null && !this.filterhourId.equals(other.filterhourId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ef.entities.FilterHourly[ filterhourId=" + filterhourId + " ]";
    }
    
}
