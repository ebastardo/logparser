/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef.model;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author erwin
 */
public class LogEntry {
    
    private String ipAddress;
    private Date date;
    private String request;
    private String comment;
    private String status;

    public LogEntry(Date date,String ipAddress,String request,String status,String comment){
        this.date=date;
        this.ipAddress=ipAddress;
        this.request=request;
        this.status=status;
        this.comment=comment;
    }
    
    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.ipAddress);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LogEntry other = (LogEntry) obj;
        return Objects.equals(this.ipAddress, other.ipAddress);
    }

    @Override
    public String toString() {
        return "LogEntry{" + "ipAddress=" + ipAddress + ", date=" + date + ", request=" + request + ", comment=" + comment + ", status=" + status + '}';
    }
    
    
    
}
