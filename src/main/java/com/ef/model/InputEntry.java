/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef.model;

/**
 *
 * @author erwin
 */
public class InputEntry {
    
     private String file;
     private  String starDate;
     private  String duration;
     private  int threshold;

    public InputEntry(String file, String starDate, String duration, int threshold) {
        this.file = file;
        this.starDate = starDate;
        this.duration = duration;
        this.threshold = threshold;
    }

    public InputEntry() {
        
    }
     
    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getStarDate() {
        return starDate;
    }

    public void setStarDate(String starDate) {
        this.starDate = starDate;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }
     
     
}
