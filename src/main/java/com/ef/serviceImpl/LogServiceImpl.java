/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef.serviceImpl;

import com.ef.entities.FilterDaily;
import com.ef.entities.FilterHourly;
import com.ef.model.LogEntry;
import com.ef.service.LogsService;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 *
 * @author erwin
 */
public class LogServiceImpl implements LogsService {

    EntityManagerFactory emf;
    EntityManager entityManager;
    EntityTransaction transaction;

    public LogServiceImpl() {
        emf = Persistence.createEntityManagerFactory("com.ef_LogParser_jar_1.0-SNAPSHOTPU");
        entityManager = emf.createEntityManager();
        transaction = entityManager.getTransaction();
    }

    @Override
    public void addLogs(List<LogEntry> logs, String duration) {

        try {
            int entityCount = logs.size() - 1;
            int batchSize = (logs.size() - 1) / 2;

            transaction.begin();
            for (int i = 0; i <= entityCount; i++) {
                if (duration.equalsIgnoreCase("hourly")) {
                    FilterHourly logHourly = new FilterHourly();
                    logHourly.setComment(logs.get(i).getComment());
                    logHourly.setDate(logs.get(i).getDate());
                    logHourly.setIpAddress(logs.get(i).getIpAddress());
                    logHourly.setRequest(logs.get(i).getRequest());
                    logHourly.setStatus(logs.get(i).getStatus());

                    if (i > 0 && i % batchSize == 0) {
                        entityManager.flush();
                        entityManager.clear();
                        transaction.commit();
                        transaction.begin();
                    }
                    entityManager.persist(logHourly);
                } else {
                    FilterDaily logDaily = new FilterDaily();
                    logDaily.setComment(logs.get(i).getComment());
                    logDaily.setDate(logs.get(i).getDate());
                    logDaily.setIpAddress(logs.get(i).getIpAddress());
                    logDaily.setRequest(logs.get(i).getRequest());
                    logDaily.setStatus(logs.get(i).getStatus());

                    if (i > 0 && i % batchSize == 0) {
                        entityManager.flush();
                        entityManager.clear();
                        transaction.commit();
                        transaction.begin();
                    }

                    entityManager.persist(logDaily);
                }
            }
            transaction.commit();
        } catch (RuntimeException e) {
            if (transaction != null
                    && transaction.isActive()) {
                transaction.rollback();
            }
            throw e;
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }

}
