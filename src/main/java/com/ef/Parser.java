/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef;

import com.ef.model.InputEntry;
import com.ef.model.LogEntry;
import com.ef.service.LogsService;
import com.ef.serviceImpl.LogServiceImpl;
import com.ef.utils.LogAnalyzerUtil;
import java.util.List;

/**
 *
 * @author erwin
 */
public class Parser {

     

    public static void main(String[] args) {
        LogsService service = new LogServiceImpl();

        InputEntry inputValues = LogAnalyzerUtil.buildInputEntry(args);
        // load the file 
        List<LogEntry> logs = LogAnalyzerUtil.loadLogs(inputValues.getFile());

        // filter to get the ipAddress that has more than the threshold asked
        LogEntry log = LogAnalyzerUtil.filterLogs(logs, inputValues.getStarDate(), inputValues.getDuration(), inputValues.getThreshold());
        //get all record for this ipaddress
        List<LogEntry> logsToDb = LogAnalyzerUtil.filterLogsByIpAddress(logs, inputValues.getStarDate(), inputValues.getDuration(), log.getIpAddress());

        //save to Db
        service.addLogs(logsToDb, inputValues.getDuration());
    }

}
